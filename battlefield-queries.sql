mysql> CREATE DATABASE battlefield;

mysql> USE battlefield;


mysql> CREATE TABLE reports (
    -> ID SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
    -> ammunition SMALLINT UNSIGNED NOT NULL,
    -> soldiers SMALLINT UNSIGNED NOT NULL,
    -> duration DOUBLE(6,1) UNSIGNED NOT NULL,
    -> critique TINYTEXT,
    -> joined TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    -> PRIMARY KEY (ID)
    -> ) engine = INNODB DEFAULT character SET = utf8 COLLATE = utf8_general_ci;

